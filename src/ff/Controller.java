package ff;


import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;


public class Controller {

    @FXML
    private TextField messageBox;

    @FXML
    private MenuItem aboutItem;
    
    @FXML
    private TimeLabel timelabelgui;
    
    private Timer timer1 = new Timer();
    private TimeLabel timelabel1;
    
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
    	
        timelabelgui = new TimeLabel(timer1);
        // initialize your logic here: all @FXML variables will have been injected

    }

    public void doAbout(ActionEvent event) {
        messageBox.setText("this is about me");
    }

    public void disable(ActionEvent event) {
        aboutItem.setDisable(true);
    }
}
