package ff;

import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application {


    private Timeline timeline;
    
    public void start(Stage stage) {

        try {

            FXMLLoader fxmlLoader = new FXMLLoader();
            String viewerFxml = "sample.fxml";
            AnchorPane page = fxmlLoader.load(this.getClass().getResource(viewerFxml).openStream());
            Scene scene = new Scene(page);
            
            stage.setScene(scene);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }
        
        
        
    }

    public static void main(String args[]) {
        launch(args);
        System.exit(0);
    }
   

}
